﻿namespace JumpOnAHeadGame.Model.Levels
{
    using System.Collections.Generic;
    using JumpOnAHeadGame.Model.Objects;
    using JumpOnAHeadGame.Model.Players;
    using JumpOnAHeadGame.View.UI;
    using Microsoft.Xna.Framework;
    public abstract class Level
    {
        public static Player Player1 { get; set; }

        public static Player Player2 { get; set; }

        public Sprite LevelBackground { get; set; }

        public List<Player> ListOfPlayers { get; set; } = new List<Player>() { Player1, Player2 };

        public List<Animation> ListOfPlayerAnimations { get; set; }

        // Snowpile + Blocks
        public List<GameObject> ListOfGameObjects { get; set; }

        public List<Snowball> ListOfSnowballs { get; set; } = new List<Snowball>();

        public List<Sprite> ListOfSnowballSprites { get; set; } = new List<Sprite>();

        public List<HealthPack> ListOfHealthPacks { get; set; }

        public List<Animation> ListOfHealthPackAnimations { get; set; }

        public abstract void Initialize();
    }
}