﻿namespace JumpOnAHeadGame.Model.Objects
{
    using Microsoft.Xna.Framework;

    public class PileOfSnow : GameObject
    {
        private const int SNOWBALL_REFILL_TIME = 30;

        public PileOfSnow(Vector2 position)
            : base(position)
        {
            this.RefillTime = 0;
        }

        public int RefillTime { get; set; }
        
        public bool IsCharged { get; set; }

        public override void ActOnCollision()
        {
            if (!this.IsCharged)
            {
                if (this.RefillTime < SNOWBALL_REFILL_TIME)
                {
                    this.RefillTime++;
                }
                else
                {
                    this.IsCharged = true;
                    this.RefillTime = 0;
                }
            }
        }
    }
}
