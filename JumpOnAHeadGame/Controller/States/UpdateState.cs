﻿namespace JumpOnAHeadGame.Controller.States
{
    using JumpOnAHeadGame.Controller.Managers;
    using JumpOnAHeadGame.Model.Objects;
    using JumpOnAHeadGame.View;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Input;
    using Model.Players;
    using View.UI;

    public class UpdateState : State
    {
        private const int MAX_PLAYER_SNOWBALLS = 5;

        public UpdateState()
            : base()
        {
            this.Initialization();
        }

        public override void Update()
        {
            if (!this.IsDone)
            {
                this.CheckForPause();

                this.PlaySound();

                this.UpdateHealthPack();

                // Using PileOfSnow
                UpdateState.CheckInsideSnowPile();

                for (int i = 0; i < StateMachine.CurrentLevel.ListOfPlayers.Count; i++)
                {
                    // Player Health Bar and Snowball counter
                    UpdateState.HandleUI(i);

                    // Base Movement, Animation and Bounds
                    UpdateState.UpdatePlayer(i);

                    // Shooting
                    this.PlayerShoot(i);
                }

                // Movement and Bounds of the Snowballs
                for (int i = 0; i < StateMachine.CurrentLevel.ListOfSnowballs.Count; i++)
                {
                    // Collision
                    this.CheckCollisonWithPlayer(i);
                    UpdateState.CheckCollisionWithBlocks(i);

                    // Movement and Destruction
                    this.SnowballMovement(i);
                }
            }
        }

        private static void CheckCollisionWithBlocks(int i)
        {
            foreach (var gameObject in StateMachine.CurrentLevel.ListOfGameObjects)
            {
                if (gameObject.GetType() == typeof(Block))
                {
                    if (StateMachine.CurrentLevel.ListOfSnowballs[i].Bounds.Intersects(gameObject.Bounds))
                    {
                        StateMachine.CurrentLevel.ListOfSnowballs[i].ActOnCollision();
                        SoundManager.Play("SnowballHitBlock");
                    }
                }
            }
        }

        private static void UpdatePlayer(int i)
        {
            StateMachine.CurrentLevel.ListOfPlayers[i].Move(StateMachine.CurrentLevel.ListOfGameObjects);
            StateMachine.CurrentLevel.ListOfPlayerAnimations[i].Update();
            StateMachine.CurrentLevel.ListOfPlayerAnimations[i].Position = StateMachine.CurrentLevel.ListOfPlayers[i].Position;
            StateMachine.CurrentLevel.ListOfPlayerAnimations[i].IsFacingRight = StateMachine.CurrentLevel.ListOfPlayers[i].IsFacingRight;
            StateMachine.CurrentLevel.ListOfPlayers[i].Bounds = new Rectangle((int)StateMachine.CurrentLevel.ListOfPlayers[i].Position.X, (int)StateMachine.CurrentLevel.ListOfPlayers[i].Position.Y,
                (int)(StateMachine.CurrentLevel.ListOfPlayerAnimations[i].SourceRectangle.Width * 0.9), StateMachine.CurrentLevel.ListOfPlayerAnimations[i].SourceRectangle.Height);
            StateMachine.CurrentLevel.ListOfPlayerAnimations[i].ChangeAnimation(StateMachine.CurrentLevel.ListOfPlayers[i].State.ToString());
        }

        private static void HandleUI(int i)
        {
            // Adjusting snowballbars
            UIInitializer.ListOfSnowballBars[i].SourceRectangle = new Rectangle(UIInitializer.ListOfSnowballBars[i].SourceRectangle.X,
                UIInitializer.ListOfSnowballBars[i].SourceRectangle.Y, 20 * StateMachine.CurrentLevel.ListOfPlayers[i].Snowballs,
                UIInitializer.ListOfSnowballBars[i].SourceRectangle.Height);

            // Adjusting Healthbars 
            UIInitializer.ListOfHealthbars[i].SourceRectangle = new Rectangle(UIInitializer.ListOfHealthbars[i].SourceRectangle.X,
                UIInitializer.ListOfHealthbars[i].SourceRectangle.Y, 3 * StateMachine.CurrentLevel.ListOfPlayers[i].Health,
                UIInitializer.ListOfHealthbars[i].SourceRectangle.Height);
        }

        private static void CheckInsideSnowPile()
        {
            foreach (var gameObject in StateMachine.CurrentLevel.ListOfGameObjects)
            {
                if (gameObject.GetType() == typeof(PileOfSnow))
                {
                    foreach (var player in StateMachine.CurrentLevel.ListOfPlayers)
                    {
                        if (gameObject.Bounds.Intersects(player.Bounds) && player.Snowballs < MAX_PLAYER_SNOWBALLS)
                        {
                            gameObject.ActOnCollision();

                            if (((PileOfSnow)gameObject).IsCharged)
                            {
                                ((PileOfSnow)gameObject).IsCharged = false;
                                player.Snowballs++;
                            }
                        }
                    }
                }
            }
        }

        private void PlayerShoot(int i)
        {
            if (StateMachine.CurrentLevel.ListOfPlayers[i].IsShooting)
            {
                StateMachine.CurrentLevel.ListOfPlayers[i].IsShooting = false;
                Vector2 snowballPosition = new Vector2();
                if (StateMachine.CurrentLevel.ListOfPlayers[i].IsFacingRight)
                {
                    snowballPosition = new Vector2(StateMachine.CurrentLevel.ListOfPlayers[i].Bounds.Right, StateMachine.CurrentLevel.ListOfPlayers[i].Position.Y + (StateMachine.CurrentLevel.ListOfPlayers[i].Bounds.Height * 0.2f));
                }
                else
                {
                    snowballPosition = new Vector2(StateMachine.CurrentLevel.ListOfPlayers[i].Bounds.Left - 40, StateMachine.CurrentLevel.ListOfPlayers[i].Position.Y + (StateMachine.CurrentLevel.ListOfPlayers[i].Bounds.Height * 0.2f));
                }

                Snowball newSnowball = new Snowball(snowballPosition, StateMachine.CurrentLevel.ListOfPlayers[i].IsFacingRight);
                StateMachine.CurrentLevel.ListOfSnowballs.Add(newSnowball);

                Sprite snowballSprite = UIInitializer.CreateSprite("Snowball");
                StateMachine.CurrentLevel.ListOfSnowballSprites.Add(snowballSprite);
                this.SpritesInState.Add(snowballSprite);
            }
        }

        private void SnowballMovement(int i)
        {
            if (!StateMachine.CurrentLevel.ListOfSnowballs[i].IsMelting)
            {
                StateMachine.CurrentLevel.ListOfSnowballs[i].Move();
                StateMachine.CurrentLevel.ListOfSnowballSprites[i].Position = StateMachine.CurrentLevel.ListOfSnowballs[i].Position;
                StateMachine.CurrentLevel.ListOfSnowballs[i].Bounds = new Rectangle(
                    (int)StateMachine.CurrentLevel.ListOfSnowballs[i].Position.X,
                    (int)StateMachine.CurrentLevel.ListOfSnowballs[i].Position.Y,
                    StateMachine.CurrentLevel.ListOfSnowballSprites[i].Texture.Width,
                    StateMachine.CurrentLevel.ListOfSnowballSprites[i].Texture.Height);
            }
            else
            {
                this.SpritesInState.Remove(StateMachine.CurrentLevel.ListOfSnowballSprites[i]);
                StateMachine.CurrentLevel.ListOfSnowballs.Remove(StateMachine.CurrentLevel.ListOfSnowballs[i]);
                StateMachine.CurrentLevel.ListOfSnowballSprites.Remove(StateMachine.CurrentLevel.ListOfSnowballSprites[i]);
            }
        }

        private void CheckCollisonWithPlayer(int i)
        {
            for (int j = 0; j < StateMachine.CurrentLevel.ListOfPlayers.Count; j++)
            {
                if (StateMachine.CurrentLevel.ListOfPlayers[j].Bounds.Intersects(StateMachine.CurrentLevel.ListOfSnowballs[i].Bounds))
                {
                    StateMachine.CurrentLevel.ListOfSnowballs[i].ActOnCollision();
                    StateMachine.CurrentLevel.ListOfPlayers[j].Health -= StateMachine.CurrentLevel.ListOfSnowballs[i].Damage;

                    if (StateMachine.CurrentLevel.ListOfPlayers[j].Health <= 0)
                    {
                        this.IsDone = true;
                        this.NextState = new GameOverState(j);
                        SoundManager.Stop(Globals.ChosenSound);
                    }
                }
            }
        }

        private void UpdateHealthPack()
        {
            for (int i = 0; i < StateMachine.CurrentLevel.ListOfHealthPacks.Count; i++)
            {
                StateMachine.CurrentLevel.ListOfHealthPackAnimations[i].Update();
                StateMachine.CurrentLevel.ListOfHealthPackAnimations[i].Position = StateMachine.CurrentLevel.ListOfHealthPacks[i].Position;
                StateMachine.CurrentLevel.ListOfHealthPacks[i].Bounds = new Rectangle((int)StateMachine.CurrentLevel.ListOfHealthPacks[i].Position.X,
                    (int)StateMachine.CurrentLevel.ListOfHealthPacks[i].Position.Y,
                    StateMachine.CurrentLevel.ListOfHealthPackAnimations[i].SourceRectangle.Width,
                    StateMachine.CurrentLevel.ListOfHealthPackAnimations[i].SourceRectangle.Height);
                if (StateMachine.CurrentLevel.ListOfHealthPacks[i].IsActive)
                {
                    if (!StateMachine.CurrentLevel.ListOfHealthPacks[i].IsDrawn)
                    {
                        this.SpritesInState.Add(StateMachine.CurrentLevel.ListOfHealthPackAnimations[i]);
                        StateMachine.CurrentLevel.ListOfHealthPacks[i].IsDrawn = true;
                    }

                    foreach (var player in StateMachine.CurrentLevel.ListOfPlayers)
                    {
                        if (StateMachine.CurrentLevel.ListOfHealthPacks[i].Bounds.Intersects(player.Bounds))
                        {
                            SoundManager.Play("HealthPack");
                            player.Health += 50;
                            if (player.Health > 100)
                            {
                                player.Health = 100;
                            }

                            StateMachine.CurrentLevel.ListOfHealthPacks[i].ActOnCollision();
                        }
                    }
                }
                else
                {
                    if (StateMachine.CurrentLevel.ListOfHealthPacks[i].IsDrawn)
                    {
                        this.SpritesInState.Remove(StateMachine.CurrentLevel.ListOfHealthPackAnimations[i]);
                        StateMachine.CurrentLevel.ListOfHealthPacks[i].IsDrawn = false;
                    }
                    else
                    {
                        if (StateMachine.CurrentLevel.ListOfHealthPacks[i].TimeToSpawn > 0)
                        {
                            StateMachine.CurrentLevel.ListOfHealthPacks[i].TimeToSpawn--;
                        }
                        else
                        {
                            StateMachine.CurrentLevel.ListOfHealthPacks[i].IsActive = true;
                            StateMachine.CurrentLevel.ListOfHealthPacks[i].TimeToSpawn = Globals.Rng.Next(500, 1200);
                        }
                    }
                }
            }
        }

        private void Initialization()
        {
            // Creating Background
            this.SpritesInState.Add(StateMachine.CurrentLevel.LevelBackground);

            //// Creating UI: names, healthbars and snowballBars
            // Player 1
            UIInitializer.Player1Name.Position = new Vector2(20, 0);
            this.SpritesInState.Add(UIInitializer.Player1Name);

            UIInitializer.HealthBarEmptyPlayer1.Position = new Vector2(175, 5);
            this.SpritesInState.Add(UIInitializer.HealthBarEmptyPlayer1);

            UIInitializer.HealthbarFullPlayer1.Position = new Vector2(180, 10);
            this.SpritesInState.Add(UIInitializer.HealthbarFullPlayer1);

            UIInitializer.SnowballBarEmptyPlayer1.Position = new Vector2(180, 45);
            this.SpritesInState.Add(UIInitializer.SnowballBarEmptyPlayer1);

            UIInitializer.SnowballBarFullPlayer1.Position = new Vector2(180, 45);
            this.SpritesInState.Add(UIInitializer.SnowballBarFullPlayer1);

            // Player 2
            UIInitializer.Player2Name.Position = new Vector2(790, 0);
            this.SpritesInState.Add(UIInitializer.Player2Name);

            UIInitializer.HealthBarEmptyPlayer2.Position = new Vector2(945, 5);
            this.SpritesInState.Add(UIInitializer.HealthBarEmptyPlayer2);

            UIInitializer.HealthbarFullPlayer2.Position = new Vector2(950, 10);
            this.SpritesInState.Add(UIInitializer.HealthbarFullPlayer2);

            UIInitializer.SnowballBarEmptyPlayer2.Position = new Vector2(950, 45);
            this.SpritesInState.Add(UIInitializer.SnowballBarEmptyPlayer2);

            UIInitializer.SnowballBarFullPlayer2.Position = new Vector2(950, 45);
            this.SpritesInState.Add(UIInitializer.SnowballBarFullPlayer2);

            // Player animation
            foreach (var animation in StateMachine.CurrentLevel.ListOfPlayerAnimations)
            {
                this.SpritesInState.Add(animation);
            }

            // Creating game objects (PileOfSnow and Blocks)
            foreach (var gameObject in StateMachine.CurrentLevel.ListOfGameObjects)
            {
                Sprite sprite = null;

                if (gameObject.GetType() == typeof(Block))
                {
                    sprite = UIInitializer.CreateSprite(((Block)gameObject).BlockType.ToString());
                }
                else if (gameObject.GetType() == typeof(PileOfSnow))
                {
                    sprite = UIInitializer.CreateSprite("PileOfSnow");
                }

                sprite.Position = gameObject.Position;
                gameObject.Bounds = new Rectangle((int)gameObject.Position.X, (int)gameObject.Position.Y, sprite.Texture.Width, sprite.Texture.Height);
                this.SpritesInState.Add(sprite);
            }

            // Coming from Pause State
            // Creating active healthpacks
            for (int i = 0; i < StateMachine.CurrentLevel.ListOfHealthPacks.Count; i++)
            {
                if (StateMachine.CurrentLevel.ListOfHealthPacks[i].IsActive)
                {
                    this.SpritesInState.Add(StateMachine.CurrentLevel.ListOfHealthPackAnimations[i]);
                    StateMachine.CurrentLevel.ListOfHealthPacks[i].IsDrawn = true;
                }
            }

            // Creating snowballs that are inflight
            foreach (var sprite in StateMachine.CurrentLevel.ListOfSnowballSprites)
            {
                this.SpritesInState.Add(sprite);
            }
        }

        private void PlaySound()
        {
            SoundState state = SoundManager.GetState(Globals.ChosenSound);
            if (state == SoundState.Stopped)
            {
                SoundManager.Play(Globals.ChosenSound);
            }
        }

        private void CheckForPause()
        {
            foreach (KeyboardButtonState key in InputHandler.ActiveKeys)
            {
                if (key.Button == Keys.Escape && key.ButtonState == KeyboardButtonState.KeyState.Clicked)
                {
                    this.IsDone = true;
                    this.NextState = new PausedState();
                }
            }
        }
    }
}