﻿namespace JumpOnAHeadGame.View
{
    using System.Collections.Generic;
    using JumpOnAHeadGame.Controller;
    using JumpOnAHeadGame.View.UI;
    using JumpOnAHeadGame.View.UI.Models;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public static class UIInitializer
    {
        public static Sprite SplashScreen { get; set; }

        public static Sprite PressAnyKey { get; set; }

        public static Sprite MenuBackground { get; set; }

        public static Sprite GameOverBackground { get; set; }

        public static Sprite PausedBackground { get; set; }

        public static Sprite CreditsSprite { get; set; }

        public static Sprite Player1WinsSprite { get; set; }

        public static Sprite Player2WinsSprite { get; set; }

        public static Sprite Player1Name { get; set; }

        public static Sprite Player2Name { get; set; }

        public static Sprite HealthBarEmptyPlayer1 { get; set; }

        public static Sprite HealthBarEmptyPlayer2 { get; set; }

        public static Sprite HealthbarFullPlayer1 { get; set; }

        public static Sprite HealthbarFullPlayer2 { get; set; }

        public static List<Sprite> ListOfHealthbars { get; set; }

        public static Sprite SnowballBarEmptyPlayer1 { get; set; }

        public static Sprite SnowballBarEmptyPlayer2 { get; set; }

        public static Sprite SnowballBarFullPlayer1 { get; set; }

        public static Sprite SnowballBarFullPlayer2 { get; set; }

        public static List<Sprite> ListOfSnowballBars { get; set; }

        public static Button StartButton { get; set; }

        public static Button ResumeButton { get; set; }

        public static Button OptionsButton { get; set; }

        public static Button CreditsButton { get; set; }

        public static Button ExitButton { get; set; }

        public static Button ExitToMenuButton { get; set; }

        public static void Initialize()
        {
            SplashScreen = CreateSprite("SplashScreen");
            PressAnyKey = CreateSprite("PressAnyKey");
            PressAnyKey.Position = new Vector2(Globals.Graphics.GraphicsDevice.Viewport.Bounds.Width / 2 - PressAnyKey.Texture.Width / 2, Globals.Graphics.GraphicsDevice.Viewport.Bounds.Height * 0.75f);
            MenuBackground = CreateSprite("MenuBackground");
            GameOverBackground = CreateSprite("GameOverBackground");
            PausedBackground = CreateSprite("PausedBackground");
            CreditsSprite = CreateSprite("Credits");
            Player1WinsSprite = CreateSprite("Player1WINS");
            Player2WinsSprite = CreateSprite("Player2WINS");
            Player1Name = CreateSprite("Player1");
            Player2Name = CreateSprite("Player2");
            HealthBarEmptyPlayer1 = CreateSprite("HealthBarEmpty");
            HealthBarEmptyPlayer2 = CreateSprite("HealthBarEmpty");
            HealthbarFullPlayer1 = CreateSprite("HealthBarFull");
            HealthbarFullPlayer2 = CreateSprite("HealthBarFull");
            ListOfHealthbars = new List<Sprite> { HealthbarFullPlayer1, HealthbarFullPlayer2 };
            SnowballBarEmptyPlayer1 = CreateSprite("SnowballBarEmpty");
            SnowballBarEmptyPlayer2 = CreateSprite("SnowballBarEmpty");
            SnowballBarFullPlayer1 = CreateSprite("SnowballBarFull");
            SnowballBarFullPlayer2 = CreateSprite("SnowballBarFull");
            ListOfSnowballBars = new List<Sprite> { SnowballBarFullPlayer1, SnowballBarFullPlayer2 };
            StartButton = CreateButton("StartNormal", "StartHover", new Vector2((Globals.Graphics.PreferredBackBufferWidth - 300) / 2, 275));
            ResumeButton = CreateButton("ResumeNormal", "ResumeHover", new Vector2((Globals.Graphics.PreferredBackBufferWidth - 300) / 2, 275));
            OptionsButton = CreateButton("OptionsNormal", "OptionsHover", new Vector2((Globals.Graphics.PreferredBackBufferWidth - 300) / 2, 375));
            CreditsButton = CreateButton("CreditsNormal", "CreditsHover", new Vector2((Globals.Graphics.PreferredBackBufferWidth - 300) / 2, 475));
            ExitButton = CreateButton("ExitNormal", "ExitHover", new Vector2((Globals.Graphics.PreferredBackBufferWidth - 300) / 2, 775));
            ExitToMenuButton = CreateButton("ExitToMenuNormal", "ExitToMenuHover", new Vector2((Globals.Graphics.PreferredBackBufferWidth - 300) / 2, 775));
        }

        public static Button CreateButton(string buttonNormal, string buttonHover, Vector2 position)
        {
            Texture2D startNormal = Globals.Content.Load<Texture2D>(buttonNormal);
            Texture2D startHover = Globals.Content.Load<Texture2D>(buttonHover);

            Sprite startSprite = new Sprite(startNormal, position);
            Button startButton = new Button(startSprite, startHover, startNormal);

            return startButton;
        }

        public static Sprite CreateSprite(string fileName)
        {
            Texture2D tileTexture = Globals.Content.Load<Texture2D>(fileName);

            Sprite tileSprite = new Sprite(tileTexture);

            return tileSprite;
        }
    }
}
