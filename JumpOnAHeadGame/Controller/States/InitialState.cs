﻿namespace JumpOnAHeadGame.Controller.States
{
    using JumpOnAHeadGame.View;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;
    using View.UI;
    public class InitialState : State
    {
        private bool shouldDraw;
        private int timer;

        public InitialState()
            : base()
        {
            this.SpritesInState.Add(UIInitializer.SplashScreen);
        }

        public override void Update()
        {
            if (!this.IsDone)
            {
                this.timer++;

                if (this.timer > 30)
                {
                    this.timer = 0;
                    this.shouldDraw = !this.shouldDraw;

                    if (this.shouldDraw)
                    {
                        this.SpritesInState.Add(UIInitializer.PressAnyKey);
                    }
                    else
                    {
                        this.SpritesInState.Remove(UIInitializer.PressAnyKey);
                    }
                }

                if (Keyboard.GetState().GetPressedKeys().Length > 0)
                {
                    this.IsDone = true;
                    this.NextState = new MenuState();
                }
            }
        }

        public override void Draw(AbstractRenderer renderer)
        {
            base.Draw(renderer);
        }
    }
}
