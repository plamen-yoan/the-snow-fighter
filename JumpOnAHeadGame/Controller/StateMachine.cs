﻿namespace JumpOnAHeadGame.Controller
{
    using System.Collections.Generic;
    using JumpOnAHeadGame.Controller.States;
    using JumpOnAHeadGame.Model.Levels;
    using Microsoft.Xna.Framework;

    public static class StateMachine
    {
        public static Level CurrentLevel { get; set; }

        public static State CurrentState { get; set; }

        public static void Initialize()
        { 
            CurrentState = new InitialState();
        }

        public static void Update()
        {
            CurrentState.Update();
            CurrentState = CurrentState.NextState;
        }
    }
}
