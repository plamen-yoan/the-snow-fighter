﻿namespace JumpOnAHeadGame.Model.Players
{
    using System.Collections.Generic;
    using Controller.Managers;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;
    using Objects;

    public enum PlayerStates
    {
        IDLE,
        RUNNING,
        WALKING
    }

    public class Player
    {
        private const float FRICTION_FORCE = 0.08f;
        private const float MAX_PLAYER_SPEED = 8;
        private const float PLAYER_ACCELERATION = 0.1f;
        private const int LEFT_BOUND = 0;
        private const int RIGHT_BOUND = 1280;
        private const int DEFAULT_SNOWBALLS = 3;
        private const int MAX_HEALTH = 100;

        public Player(Keys moveLeft, Keys moveRight, Keys jump, Keys dash, Vector2 position, bool isFacingRight)
        {
            this.State = PlayerStates.IDLE;
            this.IsFacingRight = isFacingRight;
            this.JumpHeight = 0;
            this.Controls = new Dictionary<string, Keys>();
            this.Controls.Add("Move Left", moveLeft);
            this.Controls.Add("Move Right", moveRight);
            this.Controls.Add("Jump", jump);
            this.Controls.Add("Shoot", dash);
            this.Position = position;
            this.Health = MAX_HEALTH;
            this.Snowballs = DEFAULT_SNOWBALLS;
            this.IsGrounded = false;
        }

        public Vector2 Position { get; set; }

        public Vector2 Acceleration { get; set; }

        public Rectangle Bounds { get; set; }

        public Dictionary<string, Keys> Controls { get; set; }

        public PlayerStates State { get; set; }

        public bool IsFacingRight { get; set; }

        public bool IsShooting { get; set; }

        public bool IsGrounded { get; set; }

        public float JumpHeight { get; set; }

        public int Health { get; set; }

        public int Snowballs { get; set; }

        public Vector2 Velocity { get; set; }

        public bool IsMoving { get; set; }

        public void Move(List<GameObject> gameObjects)
        {
            this.State = PlayerStates.IDLE;

            // Checks if the player is going to hit the bottom of a block
            this.HandleTopCollision(gameObjects);

            // Updates the Position according to the JumpVelocity
            this.Position += this.Velocity;

            //Jumping
            this.HandleJumping(gameObjects);

            // Moving
            this.HandleMovement(gameObjects);

            // SideCollision
            this.HandleCollision(gameObjects);
        }

        private void HandleJumping(List<GameObject> gameObjects)
        {
            // Increases the gravity
            this.Velocity = new Vector2(this.Velocity.X, this.Velocity.Y + 0.15f);

            // Checks if the player is on top of a block
            this.IsGrounded = false;

            // Bottom collision
            foreach (var block in gameObjects)
            {
                if (block.GetType() == typeof(Block))
                {
                    if (this.IsOnTopOf(block.Bounds))
                    {
                        this.Velocity = new Vector2(this.Velocity.X, 0);
                        this.IsGrounded = true;
                    }
                }
            }

            // Sets a fixed number to be the limit of the players Y.
            if (this.Position.Y > 825)
            {
                this.Position = new Vector2(this.Position.X, 825);
                this.IsGrounded = true;
            }
        }

        private void HandleTopCollision(List<GameObject> gameObjects)
        {
            if (this.Velocity.Y < 0)
            {
                foreach (var block in gameObjects)
                {
                    if (block.GetType() == typeof(Block))
                    {
                        Rectangle tempRect = new Rectangle((int)this.Bounds.X, (int)(this.Bounds.Y + this.Velocity.Y), this.Bounds.Width, 20);
                        if (tempRect.Intersects(block.Bounds))
                        {
                            this.Velocity = new Vector2(this.Velocity.X, 5);
                        }
                    }
                }
            }
        }

        private void HandleCollision(List<GameObject> gameObjects)
        {
            if ((this.Bounds.Left + (this.Bounds.Width / 2)) + this.Velocity.X < LEFT_BOUND)
            {
                this.Position = new Vector2(RIGHT_BOUND - (this.Bounds.Width / 2), this.Position.Y);
            }
            else if ((this.Bounds.Right - (this.Bounds.Width / 2)) + this.Velocity.X > RIGHT_BOUND)
            {
                this.Position = new Vector2(LEFT_BOUND - (this.Bounds.Width / 2), this.Position.Y);
            }
            else
            {
                int tempDistance;

                if (this.Velocity.X > 0)
                {
                    tempDistance = 4;
                }
                else
                {
                    tempDistance = -4;
                }

                Rectangle tempRect = new Rectangle((int)(this.Bounds.X + (this.Velocity.X + tempDistance)), (int)this.Bounds.Y, this.Bounds.Width, this.Bounds.Height);
                foreach (var block in gameObjects)
                {
                    if (block.GetType() == typeof(Block))
                    {
                        if (tempRect.Intersects(block.Bounds))
                        {
                            this.Velocity = new Vector2(0, this.Velocity.Y);
                        }
                    }
                }
            }
        }

        private void HandleMovement(List<GameObject> gameObjects)
        {
            this.IsMoving = false;
            foreach (KeyboardButtonState key in InputHandler.ActiveKeys)
            {
                if (key.Button == this.Controls["Move Left"] && key.ButtonState == KeyboardButtonState.KeyState.Held && !this.IsMoving)
                {
                    this.MoveLeft();
                }
                else if (key.Button == this.Controls["Move Right"] && key.ButtonState == KeyboardButtonState.KeyState.Held && !this.IsMoving)
                {
                    this.MoveRight();
                }
                else if (key.Button == this.Controls["Jump"] && key.ButtonState == KeyboardButtonState.KeyState.Held)
                {
                    this.Jump();
                }
                else if (key.Button == this.Controls["Shoot"] && key.ButtonState == KeyboardButtonState.KeyState.Clicked)
                {
                    this.Shoot();
                }
            }

            if (!this.IsMoving)
            {
                this.ApplyFriction();
            }
        }

        private void MoveLeft()
        {
            if (!Keyboard.GetState().IsKeyDown(this.Controls["Move Right"]))
            {
                this.IsFacingRight = false;
            }

            this.State = PlayerStates.RUNNING;
            this.IsMoving = true;
            if (this.Velocity.X > -MAX_PLAYER_SPEED)
            {
                this.Velocity = new Vector2(this.Velocity.X - PLAYER_ACCELERATION, this.Velocity.Y);
            }
        }

        private void MoveRight()
        {
            if (!Keyboard.GetState().IsKeyDown(this.Controls["Move Left"]))
            {
                this.IsFacingRight = true;
            }

            this.State = PlayerStates.RUNNING;
            this.IsMoving = true;
            if (this.Velocity.X < MAX_PLAYER_SPEED)
            {
                this.Velocity = new Vector2(this.Velocity.X + PLAYER_ACCELERATION, this.Velocity.Y);
            }
        }

        private void Jump()
        {
            if (this.IsGrounded)
            {
                this.IsGrounded = false;
                this.Velocity = new Vector2(this.Velocity.X, -9);
            }
        }

        private void Shoot()
        {
            if (this.Snowballs != 0)
            {
                this.IsShooting = true;
                this.Snowballs--;
            }
        }

        public bool IsOnTopOf(Rectangle rect)
        {
            return (this.Bounds.Bottom + this.Velocity.Y >= rect.Top - 10 &&
                this.Bounds.Bottom + this.Velocity.Y <= rect.Top &&
                this.Bounds.Right >= rect.Left + 4 &&
                this.Bounds.Left <= rect.Right - 4);
        }

        private void ApplyFriction()
        {
            if (this.Velocity.X > FRICTION_FORCE)
            {
                this.Velocity = new Vector2(this.Velocity.X - FRICTION_FORCE, this.Velocity.Y);
                this.State = PlayerStates.WALKING;
            }
            else if (this.Velocity.X < -FRICTION_FORCE)
            {
                this.Velocity = new Vector2(this.Velocity.X + FRICTION_FORCE, this.Velocity.Y);
                this.State = PlayerStates.WALKING;
            }
            else
            {
                this.Velocity = new Vector2(0, this.Velocity.Y);
            }
        }
    }
}
