﻿namespace JumpOnAHeadGame.Controller.Managers
{
    using System.Collections.Generic;
    using Microsoft.Xna.Framework.Input;

    public static class InputHandler
    {
        public static KeyboardState CurrentKeyboardState { get; set; }

        public static KeyboardState PreviousKeyboardState { get; set; }

        public static List<KeyboardButtonState> ActiveKeys { get; set; }

        public static Keys KeyToCheck { get; set; }

        public static void Initialize()
        {
            ActiveKeys = new List<KeyboardButtonState>();
        }

        public static void Update()
        {
            PreviousKeyboardState = CurrentKeyboardState;
            CurrentKeyboardState = Keyboard.GetState();

            CheckKeyState(PreviousKeyboardState, CurrentKeyboardState);
        }

        private static void CheckKeyState(KeyboardState previousState, KeyboardState currentState)
        {
            for (int i = 0; i < currentState.GetPressedKeys().Length; i++)
            {
                KeyToCheck = currentState.GetPressedKeys()[i];
                if (previousState.IsKeyUp(KeyToCheck) && currentState.IsKeyDown(KeyToCheck))
                {
                    ActiveKeys.Add(new KeyboardButtonState(KeyToCheck));
                }
                else if (previousState.IsKeyDown(KeyToCheck) && currentState.IsKeyDown(KeyToCheck))
                {
                    foreach (KeyboardButtonState key in ActiveKeys)
                    {
                        if (key.Button == KeyToCheck)
                        {
                            key.ButtonState = KeyboardButtonState.KeyState.Held;
                        }
                    }
                }
            }

            for (int i = 0; i < previousState.GetPressedKeys().Length; i++)
            {
                KeyToCheck = previousState.GetPressedKeys()[i];
                if (previousState.IsKeyDown(KeyToCheck) && currentState.IsKeyUp(KeyToCheck))
                {
                    foreach (KeyboardButtonState key in ActiveKeys)
                    {
                        if (key.Button == KeyToCheck)
                        {
                            key.ButtonState = KeyboardButtonState.KeyState.Released;
                        }
                    }
                }
            }

            for (int i = 0; i < ActiveKeys.Count; i++)
            {
                if (previousState.IsKeyUp(ActiveKeys[i].Button) && currentState.IsKeyUp(ActiveKeys[i].Button))
                {
                    ActiveKeys[i].Button = Keys.None;
                    ActiveKeys[i].ButtonState = KeyboardButtonState.KeyState.None;
                }
            }

            while (ActiveKeys.Contains(new KeyboardButtonState(Keys.None)))
            {
                ActiveKeys.Remove(new KeyboardButtonState(Keys.None));
            }
        }
    }
}
