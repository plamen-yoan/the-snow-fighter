﻿namespace JumpOnAHeadGame.Controller.States
{
    using System.Collections.Generic;
    using JumpOnAHeadGame.View;
    using JumpOnAHeadGame.View.UI;
    using Microsoft.Xna.Framework;

    public abstract class State
    {
        public State()
        {
            this.NextState = this;
            this.SpritesInState = new List<IRenderable>();
            this.IsDone = false;
        }

        public State NextState { get; set; }

        public List<IRenderable> SpritesInState { get; set; }

        protected bool IsDone { get; set; }

        public virtual void Update()
        {
        }

        public virtual void Draw(AbstractRenderer renderer)
        {
            renderer.DrawState(this.SpritesInState);
        }
    }
}
