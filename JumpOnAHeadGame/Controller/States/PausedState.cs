﻿namespace JumpOnAHeadGame.Controller.States
{
    using JumpOnAHeadGame.Controller.Managers;
    using JumpOnAHeadGame.View;
    using Microsoft.Xna.Framework.Input;

    public class PausedState : State
    {
        public PausedState()
            : base()
        {
            this.SpritesInState.Add(UIInitializer.PausedBackground);
            this.SpritesInState.Add(UIInitializer.ResumeButton.Sprite);
            this.SpritesInState.Add(UIInitializer.OptionsButton.Sprite);
            this.SpritesInState.Add(UIInitializer.ExitToMenuButton.Sprite);

            this.MenuId = 1;
        }

        public int MenuId { get; private set; }

        public override void Update()
        {
            if (!this.IsDone)
            {
                SoundManager.Pause(Globals.ChosenSound);

                SoundManager.Resume("MenuSound");

                foreach (KeyboardButtonState key in InputHandler.ActiveKeys)
                {
                    if (key.Button == Keys.Down && key.ButtonState == KeyboardButtonState.KeyState.Clicked)
                    {
                        this.MenuId++;
                        SoundManager.Play("MenuMove", 1.0f);

                        if (this.MenuId > 3)
                        {
                            this.MenuId = 1;
                        }
                    }

                    if (key.Button == Keys.Up && key.ButtonState == KeyboardButtonState.KeyState.Clicked)
                    {
                        this.MenuId--;
                        SoundManager.Play("MenuMove", 1.0f);

                        if (this.MenuId < 1)
                        {
                            this.MenuId = 3;
                        }
                    }

                    if (key.Button == Keys.Enter && key.ButtonState == KeyboardButtonState.KeyState.Clicked)
                    {
                        switch (this.MenuId)
                        {
                            case 1:
                                this.ResumeGame();
                                break;

                            case 2:
                                Globals.Graphics.ToggleFullScreen();
                                break;

                            case 3:
                                this.ExitToMenu();
                                break;
                        }
                    }
                }

                this.ChangeButtonsState();
            }
        }

        private void ExitToMenu()
        {
            this.IsDone = true;
            this.NextState = new MenuState();
        }

        private void ResumeGame()
        {
            SoundManager.Pause("MenuSound");
            SoundManager.Resume(Globals.ChosenSound);
            this.IsDone = true;
            this.NextState = new UpdateState();
        }

        private void ChangeButtonsState()
        {
            UIInitializer.ResumeButton.ChangeToNormalImage();
            UIInitializer.OptionsButton.ChangeToNormalImage();
            UIInitializer.ExitToMenuButton.ChangeToNormalImage();

            switch (this.MenuId)
            {
                case 1:
                    UIInitializer.ResumeButton.ChangeToHoverImage();
                    break;

                case 2:
                    UIInitializer.OptionsButton.ChangeToHoverImage();
                    break;

                case 3:
                    UIInitializer.ExitToMenuButton.ChangeToHoverImage();
                    break;
            }
        }
    }
}
