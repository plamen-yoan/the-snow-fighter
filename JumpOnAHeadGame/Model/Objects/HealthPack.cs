﻿namespace JumpOnAHeadGame.Model.Objects
{
    using System;
    using System.Collections.Generic;
    using JumpOnAHeadGame.Controller;
    using JumpOnAHeadGame.Controller.Managers;
    using JumpOnAHeadGame.Model.Players;
    using JumpOnAHeadGame.View.UI;
    using Microsoft.Xna.Framework;

    public class HealthPack: GameObject
    {
        public HealthPack(Vector2 position)
            :base(position)
        {
            this.Position = position;
            this.IsDrawn = false;
            this.IsActive = false;
            this.TimeToSpawn = Globals.Rng.Next(350, 700);
        }

        public Vector2 Position { get; set; }

        public Rectangle Bounds { get; set; }

        public bool IsActive { get; set; }

        public bool IsDrawn { get; set; }

        public int TimeToSpawn { get; set; }

        public override void ActOnCollision()
        {
            this.IsActive = false;
        }
    }
}
