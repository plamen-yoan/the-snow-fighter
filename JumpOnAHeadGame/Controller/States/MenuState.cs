﻿namespace JumpOnAHeadGame.Controller.States
{
    using JumpOnAHeadGame.Controller.Managers;
    using JumpOnAHeadGame.View;
    using Microsoft.Xna.Framework.Input;

    public class MenuState : State
    {
        public MenuState()
            : base()
        {
            this.SpritesInState.Add(UIInitializer.MenuBackground);
            this.SpritesInState.Add(UIInitializer.StartButton.Sprite);
            this.SpritesInState.Add(UIInitializer.OptionsButton.Sprite);
            this.SpritesInState.Add(UIInitializer.CreditsButton.Sprite);
            this.SpritesInState.Add(UIInitializer.ExitButton.Sprite);

            this.MenuId = 1;
        }

        public int MenuId { get; private set; }

        public override void Update()
        {
            if (!this.IsDone)
            {
                SoundManager.Play("MenuSound");

                foreach (KeyboardButtonState key in InputHandler.ActiveKeys)
                {
                    if (key.Button == Keys.Down && key.ButtonState == KeyboardButtonState.KeyState.Clicked)
                    {
                        this.MenuId++;
                        SoundManager.Play("MenuMove", 1.0f);

                        if (this.MenuId > 4)
                        {
                            this.MenuId = 1;
                        }
                    }

                    if (key.Button == Keys.Up && key.ButtonState == KeyboardButtonState.KeyState.Clicked)
                    {
                        this.MenuId--;
                        SoundManager.Play("MenuMove", 1.0f);

                        if (this.MenuId < 1)
                        {
                            this.MenuId = 4;
                        }
                    }

                    if (key.Button == Keys.Enter && key.ButtonState == KeyboardButtonState.KeyState.Clicked)
                    {
                        switch (this.MenuId)
                        {
                            case 1:
                                this.PlayGame();
                                break;

                            case 2:
                                Globals.Graphics.ToggleFullScreen();
                                break;

                            case 3:
                                this.GoToCredits();
                                break;

                            case 4:
                                this.ExitGame();
                                break;
                        }
                    }
                }

                this.ChangeButtonsState();
            }
        }

        private void ExitGame()
        {
            this.IsDone = true;
            SoundManager.Stop("MenuSound");
            Game1.Instance.Exit();
        }

        private void GoToCredits()
        {
            this.IsDone = true;
            this.NextState = new CreditsState();
        }

        private void PlayGame()
        {
            this.IsDone = true;
            SoundManager.Pause("MenuSound");
            Globals.ChosenSound = "Sound" + Globals.Rng.Next(1, 7).ToString();
            SoundManager.Play(Globals.ChosenSound);

            int randomLevel = 0;
            do
            {
                randomLevel = Globals.Rng.Next(0, 3);
            }
            while (Globals.ListOfLevels[randomLevel] == StateMachine.CurrentLevel);

            StateMachine.CurrentLevel = Globals.ListOfLevels[randomLevel];
            StateMachine.CurrentLevel.Initialize();

            this.NextState = new UpdateState();
        }

        private void ChangeButtonsState()
        {
            UIInitializer.StartButton.ChangeToNormalImage();
            UIInitializer.OptionsButton.ChangeToNormalImage();
            UIInitializer.CreditsButton.ChangeToNormalImage();
            UIInitializer.ExitButton.ChangeToNormalImage();

            switch (this.MenuId)
            {
                case 1:
                    UIInitializer.StartButton.ChangeToHoverImage();
                    break;

                case 2:
                    UIInitializer.OptionsButton.ChangeToHoverImage();
                    break;

                case 3:
                    UIInitializer.CreditsButton.ChangeToHoverImage();
                    break;

                case 4:
                    UIInitializer.ExitButton.ChangeToHoverImage();
                    break;
            }
        }
    }
}
