﻿namespace JumpOnAHeadGame.Model.Objects
{
    using System.Collections.Generic;
    using JumpOnAHeadGame.Model.Players;
    using JumpOnAHeadGame.View.UI;
    using Microsoft.Xna.Framework;

    public enum BlockType
    {
        IceCube,
        IceBlock
    }

    public class Block : GameObject
    {
        public Block(Vector2 position, BlockType type)
            : base(position)
        {
            this.BlockType = type;
        }

        public BlockType BlockType { get; set; }

        public override void ActOnCollision()
        {
            throw new System.NotImplementedException();
        }
    }
}
